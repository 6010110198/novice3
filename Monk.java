
package novice3;

public class Monk {
    public int exp;
    public int hp;
    public int level;
    public int hpm;
    
    public Monk(){
        exp = 0;
        hp = 100;
        level = 1;
        hpm = 100;
}
    public void print(){
        System.out.println("------------------------");
        System.out.println("Level: " + this.level);
        System.out.println("exp: " + this.exp);
        System.out.println("HP: " + this.hp);
        System.out.println("HPMonster: " + this.hpm);
        System.out.println("------------------------");
    }
     public void PlayerMonk(){
        this.hp -= 5;
        exp += 20;
        hpm -= 10;
        if(this.hp <= 20){
            System.out.println("Be carefull");
            print();
        }
        if(exp == 100){
            this.level += 1 ;
            exp = 0;
            System.out.println("Level up");
        }
        print();
        
    }
     public int statusHp(){
        return hp;
    }
    public int statusHpm(){
        return hpm;
    }
    public void useItem(int power){
        if(power == 50){
            hp = hp + power ;
        }
        else if(power == 1){
            level = level + power ;
        }
        else{
            exp = exp + power ;
        }
        print();
    }
}

    
    
    
