package novice3;
import java.io.*;
public class Thief {
   
    public int hp ;
    public int exp ;
    public int level;
    public int hpm ;
    
    public Thief(){
        hp = 100;
        hpm = 100;
        exp = 0;
        level = 1;
    }
    public void print(){
        
        System.out.println("------------------------");
        System.out.println("Level: " + this.level);
        System.out.println("exp: " + this.exp);
        System.out.println("HP: " + this.hp);
        System.out.println("HPMonster: " + this.hpm);
        System.out.println("------------------------");
    
    }
    public void PlayerThief(){
        this.hp -= 20;
        hpm -= 30;
        exp += 10;
       
        if(this.hp <= 20){
            System.out.println("Be carefull");
        }
        if(exp == 100){
            this.level += 1 ;
            exp = 0;
            System.out.println("Level up");

        }
        print();
    }
     public int Statushp(){
        return hp;
    }
    public int Statushpm(){
        return hpm;
    }
    public void useItem(int power){
        
        if(power == 50){
            hp = hp + power ;
        }
        else if(power == 1){
            level = level + power ;
        }
        else{
            exp = exp + power ;
        }
        print();
    }
}


